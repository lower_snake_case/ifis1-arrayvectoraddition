#ifndef CVECTOR_HPP
#define CVECTOR_HPP


class CVector
{
private:
    double md_XCoordinate;
    double md_YCoordinate;
    double md_ZCoordinate;

public:
    CVector();
    CVector(const double & theXCoordinate,
            const double & theYCoordinate,
            const double & theZCoordinate = 0);

    double  GetXCoordinate() const;
    void    SetXCoordinate(const double & theXCoordinate);

    double  GetYCoordinate() const;
    void    SetYCoordinate(const double & theYCoordinate);

    double  GetZCoordinate() const;
    void    SetZCoordinate(const double & theZCoordinate);

    CVector GetVectorAddition (const CVector & theVectorToAdd) const;

    void    PrintCoordinates() const;

    // TODO: int GetSizeOf () const;
};

#endif // CVECTOR_HPP
