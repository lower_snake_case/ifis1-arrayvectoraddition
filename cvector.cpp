#include <iostream>

#include "cvector.hpp"

/**
 * @brief CVector::CVector
 */
CVector::CVector()
{
}

/**
 * @brief CVector::CVector
 * @param theXCoordinate
 * @param theYCoordinate
 * @param theZCoordinate
 */
CVector::CVector(const double & theXCoordinate,
                 const double & theYCoordinate,
                 const double & theZCoordinate)
{
    this->md_XCoordinate = theXCoordinate;
    this->md_YCoordinate = theYCoordinate;
    this->md_ZCoordinate = theZCoordinate;

}

/**
 * @brief CVector::GetXCoordinate
 * @return md_XCoordinate
 */
double CVector::GetXCoordinate() const
{
    return this->md_XCoordinate;

}

/**
 * @brief CVector::SetXCoordinate
 * @param theXCoordinate
 */
void CVector::SetXCoordinate(const double & theXCoordinate)
{
    this->md_XCoordinate = theXCoordinate;

}

/**
 * @brief CVector::GetYCoordinate
 * @return md_YCoordinate
 */
double CVector::GetYCoordinate() const
{
    return this->md_YCoordinate;

}

/**
 * @brief CVector::SetYCoordinate
 * @param theYCoordinate
 */
void CVector::SetYCoordinate(const double & theYCoordinate)
{
    this->md_YCoordinate = theYCoordinate;

}

/**
 * @brief CVector::GetZCoordinate
 * @return md_ZCoordinate
 */
double CVector::GetZCoordinate() const
{
    return this->md_ZCoordinate;

}

/**
 * @brief CVector::SetZCoordinate
 * @param theZCoordinate
 */
void CVector::SetZCoordinate(const double & theZCoordinate)
{
    md_ZCoordinate = theZCoordinate;
}

/**
 * @brief CVector::GetVectorAddition
 * @param  theVectorToAdd
 * @return Resulting Vector
 */
CVector CVector::GetVectorAddition(const CVector & theVectorToAdd) const
{
    const double ld_XCoordinate = this->md_XCoordinate + theVectorToAdd.GetXCoordinate();
    const double ld_YCoordinate = this->md_YCoordinate + theVectorToAdd.GetYCoordinate();
    const double ld_ZCoordinate = this->md_ZCoordinate + theVectorToAdd.GetZCoordinate();

    return CVector(ld_XCoordinate, ld_YCoordinate, ld_ZCoordinate);

}

/**
 * @brief CVector::PrintCoordinates
 */
void CVector::PrintCoordinates() const
{
   std::cout << "X:\t" << this->md_XCoordinate << "\n"
             << "Y:\t" << this->md_YCoordinate << "\n"
             << "Z:\t" << this->md_ZCoordinate << "\n";

}
