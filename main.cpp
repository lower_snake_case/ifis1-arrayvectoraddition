#include <iostream>
#include <ctime>

#include <unistd.h>

#include "cvector.hpp"

double  GetRandom();
CVector GetRandomVector();
void PrintAddidition(CVector    theVectorGroup [],
                     const int  theBaseVectorIndex,
                     const int  theVectorToAddIndex,
                     const int  theArraySize);

int main()
{
    CVector lo_VectorGroup [5];

    for (int i = 0; i < 5; ++i)
        lo_VectorGroup[i] = GetRandomVector();

    PrintAddidition(lo_VectorGroup, 0, 0, 5);
    return 0;

}

// TODO: Calculate lenght of Array

double GetRandom() // TODO: create real random
{
    srand((unsigned) std::time(NULL));
    sleep(1);
    return ((random() % 100) / 10.0);

}

CVector GetRandomVector()
{
    return CVector (GetRandom(), GetRandom(), GetRandom());

}

void PrintAddidition(CVector    theVectorGroup [],
                     const int  theBaseVectorIndex,
                     const int  theVectorToAddIndex,
                     const int  theArraySize)
{
    CVector lo_ResultVector = theVectorGroup[theBaseVectorIndex].GetVectorAddition(theVectorGroup[theVectorToAddIndex]);
    lo_ResultVector.PrintCoordinates();

    if (theBaseVectorIndex < theArraySize) {
        if (theVectorToAddIndex < theArraySize) {

            PrintAddidition(theVectorGroup, theBaseVectorIndex, theVectorToAddIndex + 1, theArraySize);

        }
        else {

            PrintAddidition(theVectorGroup, theBaseVectorIndex + 1, 0, theArraySize);

        }
    }

}
